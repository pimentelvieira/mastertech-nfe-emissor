package com.mastertech.nfe.emissor.impostos;

import com.mastertech.nfe.cadastro.models.NFE;

public class NFEPessoaFisica implements NFEImposto {

    @Override
    public NFE calcular(NFE nfe) {
        nfe.setCofins(0.0);
        nfe.setCsll(0.0);
        nfe.setIrrf(0.0);
        nfe.setValorFinal(nfe.getValor());
        return nfe;
    }
}
