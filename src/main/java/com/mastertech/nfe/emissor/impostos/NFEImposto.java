package com.mastertech.nfe.emissor.impostos;

import com.mastertech.nfe.cadastro.models.NFE;

public interface NFEImposto {

    double ALIQUOTA_IRRF = 0.015;
    double ALIQUOTA_CSLL = 0.03;
    double ALIQUOTA_COFINS = 0.0065;

    NFE calcular(NFE nfe);
}
