package com.mastertech.nfe.emissor.impostos;

import com.mastertech.nfe.cadastro.models.NFE;

public class NFEPessoaJuridicaNaoOptanteSN implements NFEImposto {

    @Override
    public NFE calcular(NFE nfe) {
        nfe.setCofins(nfe.getValor() * NFEImposto.ALIQUOTA_COFINS);
        nfe.setCsll(nfe.getValor() * NFEImposto.ALIQUOTA_CSLL);
        nfe.setIrrf(nfe.getValor() * NFEImposto.ALIQUOTA_IRRF);
        nfe.setValorFinal(nfe.getValor() - nfe.getCofins() - nfe.getCsll() - nfe.getIrrf());
        return nfe;
    }
}
