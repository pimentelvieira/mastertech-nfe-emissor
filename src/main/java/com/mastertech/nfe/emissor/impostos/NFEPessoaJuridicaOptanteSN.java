package com.mastertech.nfe.emissor.impostos;

import com.mastertech.nfe.cadastro.models.NFE;

public class NFEPessoaJuridicaOptanteSN implements NFEImposto {

    @Override
    public NFE calcular(NFE nfe) {
        nfe.setCofins(0.0);
        nfe.setCsll(0.0);
        nfe.setIrrf(nfe.getValor() * NFEImposto.ALIQUOTA_IRRF);
        nfe.setValorFinal(nfe.getValor() - nfe.getIrrf());
        return nfe;
    }
}
