package com.mastertech.nfe.emissor.services;

import com.mastertech.nfe.cadastro.models.NFE;
import com.mastertech.nfe.emissor.feignclients.ConsultaCNPJFeignClient;
import com.mastertech.nfe.emissor.feignclients.NFECadastroMicroserviceFeignClient;
import com.mastertech.nfe.emissor.feignclients.dtos.ConsultaCnpjResponseDTO;
import com.mastertech.nfe.emissor.feignclients.mappers.UpdateNFEDTOMapper;
import com.mastertech.nfe.emissor.impostos.NFEImposto;
import com.mastertech.nfe.emissor.utils.IdentidadeUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class EmissorNFEService {

    @Autowired
    private ConsultaCNPJFeignClient consultaCNPJFeignClient;

    @Autowired
    private NFECadastroMicroserviceFeignClient nfeCadastroMicroserviceFeignClient;

    public void emitirNFE(NFE nfe) {
        System.out.println("Inicio nfe-emissor consumer");
        NFEImposto nfeImposto;
        if(IdentidadeUtil.isCnpj(nfe.getIdentidade())) {
            ConsultaCnpjResponseDTO cnpj = consultaCNPJFeignClient.getByCnpj(nfe.getIdentidade());
            nfeImposto = IdentidadeUtil.getTipoNFE(nfe.getIdentidade(), Double.parseDouble(cnpj.getCapitalSocial()));
        } else {
            nfeImposto = IdentidadeUtil.getTipoNFE(nfe.getIdentidade(), null);
        }
        NFE nfeCalculada = nfeImposto.calcular(nfe);
        this.nfeCadastroMicroserviceFeignClient.updateNFE(UpdateNFEDTOMapper.toUpdateNFEDTO(nfeCalculada), nfe.getId());
        System.out.println("Fim nfe-emissor consumer");
    }
}
