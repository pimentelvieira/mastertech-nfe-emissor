package com.mastertech.nfe.emissor.utils;

import com.mastertech.nfe.emissor.impostos.NFEImposto;
import com.mastertech.nfe.emissor.impostos.NFEPessoaFisica;
import com.mastertech.nfe.emissor.impostos.NFEPessoaJuridicaNaoOptanteSN;
import com.mastertech.nfe.emissor.impostos.NFEPessoaJuridicaOptanteSN;

public class IdentidadeUtil {

    public static boolean isCnpj(String identidade) {
        return identidade.length() == 14;
    }

    public static NFEImposto getTipoNFE(String identidade, Double capitalSocial) {
        if (!isCnpj(identidade)) {
            return new NFEPessoaFisica();
        } else {
            if (capitalSocial < 1000000) {
                return new NFEPessoaJuridicaOptanteSN();
            } else {
                return new NFEPessoaJuridicaNaoOptanteSN();
            }
        }
    }
}
