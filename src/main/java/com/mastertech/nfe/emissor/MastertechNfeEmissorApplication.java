package com.mastertech.nfe.emissor;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;

@SpringBootApplication
@EnableFeignClients
public class MastertechNfeEmissorApplication {

	public static void main(String[] args) {
		SpringApplication.run(MastertechNfeEmissorApplication.class, args);
	}

}
