package com.mastertech.nfe.emissor.consumers;

import com.mastertech.nfe.cadastro.models.NFE;
import com.mastertech.nfe.emissor.feignclients.dtos.ConsultaCnpjResponseDTO;
import com.mastertech.nfe.emissor.impostos.NFEImposto;
import com.mastertech.nfe.emissor.services.EmissorNFEService;
import com.mastertech.nfe.emissor.utils.IdentidadeUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Component;

import java.io.IOException;

@Component
public class NFEConsumer {

    @Autowired
    private EmissorNFEService emissorNFEService;

    @KafkaListener(topics = "spec2-william-pimentel-1", groupId = "nfe-emissor")
    public void receber(@Payload NFE nfe) throws IOException {
        this.emissorNFEService.emitirNFE(nfe);
    }
}
