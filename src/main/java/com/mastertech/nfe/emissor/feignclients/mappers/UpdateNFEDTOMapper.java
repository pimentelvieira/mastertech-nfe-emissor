package com.mastertech.nfe.emissor.feignclients.mappers;

import com.mastertech.nfe.cadastro.models.NFE;
import com.mastertech.nfe.emissor.feignclients.dtos.UpdateNFEDTO;

public class UpdateNFEDTOMapper {

    public static UpdateNFEDTO toUpdateNFEDTO(NFE nfe) {
        UpdateNFEDTO updateNFEDTO = new UpdateNFEDTO();
        updateNFEDTO.setValorCofins(nfe.getCofins());
        updateNFEDTO.setValorCSLL(nfe.getCsll());
        updateNFEDTO.setValorFinal(nfe.getValorFinal());
        updateNFEDTO.setValorIRRF(nfe.getIrrf());;

        return updateNFEDTO;
    }
}
