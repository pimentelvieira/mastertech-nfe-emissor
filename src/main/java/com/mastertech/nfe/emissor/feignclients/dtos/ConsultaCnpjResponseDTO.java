package com.mastertech.nfe.emissor.feignclients.dtos;

import com.fasterxml.jackson.annotation.JsonProperty;

public class ConsultaCnpjResponseDTO {

    @JsonProperty("capital_social")
    private String capitalSocial;

    public String getCapitalSocial() {
        return capitalSocial;
    }

    public void setCapitalSocial(String capitalSocial) {
        this.capitalSocial = capitalSocial;
    }

}
