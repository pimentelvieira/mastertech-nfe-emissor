package com.mastertech.nfe.emissor.feignclients;

import com.mastertech.nfe.emissor.feignclients.dtos.ConsultaCnpjResponseDTO;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient(name = "consultacnpj", url = "https://www.receitaws.com.br/v1/cnpj")
public interface ConsultaCNPJFeignClient {

    @GetMapping("/{cnpj}")
    ConsultaCnpjResponseDTO getByCnpj(@PathVariable String cnpj);
}

