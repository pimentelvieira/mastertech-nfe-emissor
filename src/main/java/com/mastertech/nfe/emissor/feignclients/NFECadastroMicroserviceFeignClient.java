package com.mastertech.nfe.emissor.feignclients;

import com.mastertech.nfe.emissor.feignclients.dtos.UpdateNFEDTO;
import com.mastertech.nfe.emissor.security.OAuth2FeignConfiguration;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;

@FeignClient(name = "nfe-cadastro-microservice", configuration = OAuth2FeignConfiguration.class)
public interface NFECadastroMicroserviceFeignClient {

    @PutMapping("/nfe/{id}")
    void updateNFE(@RequestBody UpdateNFEDTO updateNFEDTO, @PathVariable Integer id);
}