package com.mastertech.nfe.emissor.feignclients.dtos;

public class UpdateNFEDTO {

    private Double valorIRRF;
    private Double valorCSLL;
    private Double valorCofins;
    private Double valorFinal;

    public Double getValorIRRF() {
        return valorIRRF;
    }

    public void setValorIRRF(Double valorIRRF) {
        this.valorIRRF = valorIRRF;
    }

    public Double getValorCSLL() {
        return valorCSLL;
    }

    public void setValorCSLL(Double valorCSLL) {
        this.valorCSLL = valorCSLL;
    }

    public Double getValorCofins() {
        return valorCofins;
    }

    public void setValorCofins(Double valorCofins) {
        this.valorCofins = valorCofins;
    }

    public Double getValorFinal() {
        return valorFinal;
    }

    public void setValorFinal(Double valorFinal) {
        this.valorFinal = valorFinal;
    }
}
